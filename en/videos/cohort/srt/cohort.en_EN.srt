﻿1
00:00:05,000 --> 00:00:09,000
We're going to walk through how to create a cohort in Facebook

2
00:00:09,000 --> 00:00:13,000
Analytics for Apps. First, Click on the "Activity" section

3
00:00:13,000 --> 00:00:17,000
on the left menu. Next, you want to click on 

4
00:00:17,000 --> 00:00:21,000
"Cohorts." In the upper-right

5
00:00:21,000 --> 00:00:25,000
corner, you're going to want to click "Create New."

6
00:00:25,000 --> 00:00:29,000
Next, you want to enter

7
00:00:29,000 --> 00:00:33,000
a cohort name that describes the type of cohort. In our case, 

8
00:00:33,000 --> 00:00:37,000
we're going to create a cohort named "Install

9
00:00:37,000 --> 00:00:41,000
to Purchase." 

10
00:00:41,000 --> 00:00:45,000
In the "Show People Who" section, you want to select a specific app event

11
00:00:45,000 --> 00:00:49,000
that is the first step that people are taking.

12
00:00:49,000 --> 00:00:53,000
Let's do "App Installs." And then, in the next section,

13
00:00:53,000 --> 00:00:57,000
"And Then Later," we'll choose our next app event, which is 

14
00:00:57,000 --> 00:01:01,000
"Purchases."

15
00:01:01,000 --> 00:01:05,000


16
00:01:05,000 --> 00:01:09,000
Finally, we'll click "Save."

17
00:01:09,000 --> 00:01:13,000


18
00:01:13,000 --> 00:01:17,000
So, we can now look at data from cohorts that shows us the percentage

19
00:01:17,000 --> 00:01:21,000
of people that are purchasing on each day.

20
00:01:21,000 --> 00:01:25,000
Because this purchase event has a value associated with it, we 

21
00:01:25,000 --> 00:01:29,000
should change "Chart Type" from "User Activity" to "Cumulative Values."

22
00:01:29,000 --> 00:01:33,000
But first, if you scroll down, 

23
00:01:33,000 --> 00:01:37,000
you can see the percentage values.

24
00:01:37,000 --> 00:01:41,000
If I change "Chart Type" from "User Activity" to "Cumulative Values,"

25
00:01:41,000 --> 00:01:45,000
I can now see the number of spends per user

26
00:01:45,000 --> 00:01:49,000
over time. What that tells me is that over time,

27
00:01:49,000 --> 00:01:53,000
the amount of people spending on your app is growing.

28
00:01:53,000 --> 00:01:57,000
Use this to understand things like lifetime value. If you scroll down,

29
00:01:57,000 --> 00:02:01,000
you can see this on a daily basis, to see, of people

30
00:02:01,000 --> 00:02:05,000
who are installing your app on a given day, how people are coming back

31
00:02:05,000 --> 00:02:09,000
multiple days later. And you can use this

32
00:02:09,000 --> 00:02:13,000
to optimize your app for additional revenue.

33
00:02:13,000 --> 00:02:15,266


