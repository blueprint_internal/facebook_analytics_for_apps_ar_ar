﻿1
00:00:05,000 --> 00:00:09,000
Para crear un embudo en Facebook Analytics for Apps

2
00:00:09,000 --> 00:00:13,000
haz clic en "Embudos" debajo de la sección "Actividad".

3
00:00:13,000 --> 00:00:17,000
A continuación, haz clic en "Crear embudo".

4
00:00:17,000 --> 00:00:21,000
Para agregarle pasos

5
00:00:21,000 --> 00:00:25,000
haz clic en "Agregar paso de embudo". En este caso, crearemos

6
00:00:25,000 --> 00:00:29,000
un embudo que represente un proceso comercial normal. Hago clic 

7
00:00:29,000 --> 00:00:33,000
en "Agregar paso de embudo" y busco uno.

8
00:00:33,000 --> 00:00:37,000
Agrego otro paso:

9
00:00:37,000 --> 00:00:41,000
"Agregar al carrito". Por último, incluiré

10
00:00:41,000 --> 00:00:45,000
el paso "Compras". Ten en cuenta 

11
00:00:45,000 --> 00:00:49,000
que puedes acotar algunos de estos pasos haciendo clic en "Seleccionar parámetro".

12
00:00:49,000 --> 00:00:53,000
Por ejemplo, puedes seleccionar "Evento de compra

13
00:00:53,000 --> 00:00:57,000
superior a 50 USD". Después de esto,

14
00:00:57,000 --> 00:01:01,000
hago clic en "Acotar" para cerrarlo

15
00:01:01,000 --> 00:01:05,000
y en "Aplicar" para crear el embudo.

17
00:01:09,000 --> 00:01:13,000
Una vez creado,

18
00:01:13,000 --> 00:01:17,000
puedes ver la diferencia general entre los pasos.

19
00:01:17,000 --> 00:01:21,000
Si te desplazas hacia abajo, verás los valores numéricos 

20
00:01:21,000 --> 00:01:25,000
de las tasas de conversión y el número de personas que realizan cada paso del embudo.

21
00:01:25,000 --> 00:01:29,000
Puedes mostrar el embudo en función de distintos atributos.

22
00:01:29,000 --> 00:01:33,000
Por ejemplo, puedes desglosarlo por la edad o el sexo, y ver distintas tasas de conversión.

23
00:01:33,000 --> 00:01:37,000
Para aplicar un segmento a tu embudo, selecciona

24
00:01:37,000 --> 00:01:41,000
la lista desplegable de segmentos. También puedes 

25
00:01:41,000 --> 00:01:45,000
hacer clic en "Guardar" para guardar el embudo. 

26
00:01:45,000 --> 00:01:49,000
Asígnale un nombre y haz clic en "Guardar como".

27
00:01:49,000 --> 00:01:53,000
Así se crea un embudo

28
00:01:53,000 --> 00:01:56,900
en Facebook Analytics for Apps.