﻿1
00:00:05,000 --> 00:00:09,000
Vous pouvez créer un entonnoir dans 
Facebook Analytics pour apps

2
00:00:09,000 --> 00:00:13,000
en cliquant sur Entonnoirs dans la section Activité.

3
00:00:13,000 --> 00:00:17,000
Ensuite, cliquez sur Créer l’entonnoir.

4
00:00:17,000 --> 00:00:21,000
Pour ajouter des étapes à votre entonnoir,

5
00:00:21,000 --> 00:00:25,000
cliquez sur Ajouter une étape d’entonnoir.
Dans le cas présent, nous allons créer

6
00:00:25,000 --> 00:00:29,000
un entonnoir qui représente un flux commercial
type.

7
00:00:29,000 --> 00:00:33,000
Je clique sur Ajouter une étape d’entonnoir
et je recherche une étape.

8
00:00:33,000 --> 00:00:37,000
J’ajoute une nouvelle étape d’entonnoir :

9
00:00:37,000 --> 00:00:41,000
Ajouter au panier.
Ensuite, j’ajoute l’étape finale

10
00:00:41,000 --> 00:00:45,000
Achats.
Prenez note :

11
00:00:45,000 --> 00:00:49,000
pour affiner certaines de ces étapes,
vous pouvez cliquer sur

12
00:00:49,000 --> 00:00:53,000
Sélectionner un paramètre
et choisir des options telles que

13
00:00:53,000 --> 00:00:57,000
Évènement d’achat supérieur à 50 $.
Lorsque j’ai terminé,

14
00:00:57,000 --> 00:01:01,000
je peux cliquer sur Affiner
pour fermer le paramètre

15
00:01:01,000 --> 00:01:05,000
et cliquer sur Appliquer pour créer mon entonnoir.

17
00:01:09,000 --> 00:01:13,000
Lorsque l’entonnoir est créé,

18
00:01:13,000 --> 00:01:17,000
vous pouvez observer
la différence totale entre les étapes.

19
00:01:17,000 --> 00:01:21,000
Vous pouvez faire défiler la page vers le bas pour
voir les valeurs numériques

20
00:01:21,000 --> 00:01:25,000
pour les taux de conversion, et le nombre de personnes
qui terminent chaque étape de l’entonnoir.

21
00:01:25,000 --> 00:01:29,000
Vous pouvez également afficher l’entonnoir
selon différents attributs.

22
00:01:29,000 --> 00:01:33,000
Vous pouvez faire une répartition par âge ou par
sexe, par exemple, et voir différents taux de
conversion.

23
00:01:33,000 --> 00:01:37,000
Pour appliquer un segment à votre entonnoir,
vous pouvez sélectionner

24
00:01:37,000 --> 00:01:41,000
le menu déroulant des segments.
Vous pouvez aussi 

25
00:01:41,000 --> 00:01:45,000
cliquer sur Enregistrer
pour enregistrer l’entonnoir.

26
00:01:45,000 --> 00:01:49,000
Saisissez un nom et cliquez sur Enregistrer sous.

27
00:01:49,000 --> 00:01:53,000
Voilà comment créer un entonnoir

28
00:01:53,000 --> 00:01:56,900
dans Facebook Analytics pour apps.