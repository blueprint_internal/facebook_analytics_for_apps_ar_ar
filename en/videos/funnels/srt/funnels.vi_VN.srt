﻿1
00:00:05,000 --> 00:00:09,000
Bạn có thể tạo phễu trong Phân tích ứng dụng trên Facebook bằng cách

2
00:00:09,000 --> 00:00:13,000
nhấp vào "Phễu" trong phần "Hoạt động".

3
00:00:13,000 --> 00:00:17,000
Tiếp theo, nhấp vào "Tạo phễu".

4
00:00:17,000 --> 00:00:21,000
Bạn có thể thêm các bước vào phễu bằng cách

5
00:00:21,000 --> 00:00:25,000
nhấp vào "Thêm bước phễu". Trong trường hợp này, chúng ta sẽ tạo một

6
00:00:25,000 --> 00:00:29,000
phễu đại diện cho một quy trình thương mại điển hình. Vì vậy tôi sẽ nhấp vào 

7
00:00:29,000 --> 00:00:33,000
"Thêm bước phễu", sau đó tìm kiếm bước phễu.

8
00:00:33,000 --> 00:00:37,000
Tôi sẽ thêm một bước phễu khác,

9
00:00:37,000 --> 00:00:41,000
đó là "Thêm vào giỏ hàng". Sau đó, tôi sẽ thêm bước phễu

10
00:00:41,000 --> 00:00:45,000
cuối cùng, đó là "Mua hàng". Lưu ý như sau: 

11
00:00:45,000 --> 00:00:49,000
thực tế, bạn có thể tinh chỉnh một số bước trong số này bằng cách nhấp vào "Chọn thông số"

12
00:00:49,000 --> 00:00:53,000
và bạn có thể chọn các nội dung như "Sự kiện

13
00:00:53,000 --> 00:00:57,000
mua lớn hơn $50". Sau khi tôi đã

14
00:00:57,000 --> 00:01:01,000
hoàn tất bước đó, tôi có thể nhấp vào "Tinh chỉnh" để đóng, sau đó

15
00:01:01,000 --> 00:01:05,000
nhấp vào "Áp dụng" để tạo phễu.

17
00:01:09,000 --> 00:01:13,000
Sau khi tạo phễu,

18
00:01:13,000 --> 00:01:17,000
bạn có thể xem sự khác biệt tổng thể giữa các bước.

19
00:01:17,000 --> 00:01:21,000
Bạn có thể cuộn xuống để xem các giá trị số cho 

20
00:01:21,000 --> 00:01:25,000
tỷ lệ chuyển đổi, cũng như số người hoàn thành từng bước trong phễu.

21
00:01:25,000 --> 00:01:29,000
Bạn cũng có thể hiển thị phễu theo các thuộc tính khác nhau.

22
00:01:29,000 --> 00:01:33,000
Vì vậy hãy phân tích phễu theo các tiêu chí như độ tuổi hoặc giới tính, sau đó xem các tỷ lệ chuyển đổi khác nhau.

23
00:01:33,000 --> 00:01:37,000
Bạn có thể áp dụng phân đoạn vào phễu bằng cách chọn

24
00:01:37,000 --> 00:01:41,000
menu phân đoạn thả xuống. Bạn cũng có thể 

25
00:01:41,000 --> 00:01:45,000
lưu phễu bằng cách nhấp vào "Lưu". 

26
00:01:45,000 --> 00:01:49,000
Nhập tên cho phễu rồi nhấp vào "Lưu dưới dạng".

27
00:01:49,000 --> 00:01:53,000
Và đó là cách tạo phễu

28
00:01:53,000 --> 00:01:56,900
trong Phân tích ứng dụng trên Facebook.