﻿1
00:00:05,000 --> 00:00:09,000
It's easy to set up a segment in

2
00:00:09,000 --> 00:00:13,000
Facebook Analytics for Apps, in a number of different reports. I'm going to walk

3
00:00:13,000 --> 00:00:17,000
you through how to set up a segment in the "Revenue" report.

4
00:00:17,000 --> 00:00:21,000
Click on "Revenue," and

5
00:00:21,000 --> 00:00:25,000
I can see that currently the report isn't filtered 

6
00:00:25,000 --> 00:00:29,000
at all, and is showing 100% of the users. But, I can 

7
00:00:29,000 --> 00:00:33,000
apply a segment by clicking "Edit."

8
00:00:33,000 --> 00:00:37,000
Once I click "Edit," I can click "Select Condition," and select from

9
00:00:37,000 --> 00:00:41,000
different types of conditions, including "Event," "Demographics," 

10
00:00:41,000 --> 00:00:45,000
"Device Info" or "Install Source." I can also 

11
00:00:45,000 --> 00:00:49,000
add a percentile condition to filter based on

12
00:00:49,000 --> 00:00:53,000
people who take a certain action the most. In this example, I'm going to 

13
00:00:53,000 --> 00:00:57,000
create a condition based on demographics.

14
00:00:57,000 --> 00:01:01,000
I'm going to select a parameter of 

15
00:01:01,000 --> 00:01:05,000
"Country." I'm going to 

16
00:01:05,000 --> 00:01:09,000
click into the field and select "United States of America."

17
00:01:09,000 --> 00:01:13,000
And then I'm going to click "Add Percentile."

18
00:01:13,000 --> 00:01:17,000
Once I click that, I'm going to select the event that I want to base the percentile on.

19
00:01:17,000 --> 00:01:21,000
Let's say people who launch the app, 

20
00:01:21,000 --> 00:01:25,000
and are part of the top 10% of people.

21
00:01:25,000 --> 00:01:29,000
Next, I'm going to click "Apply."

22
00:01:29,000 --> 00:01:33,000
Now you can see the chart is

23
00:01:33,000 --> 00:01:37,000
updating to show me the data for this segment. 

24
00:01:37,000 --> 00:01:41,000
This segment isn't yet saved, so I can 

25
00:01:41,000 --> 00:01:45,000
click "More" and select "Save As." Now I can give 

26
00:01:45,000 --> 00:01:49,000
my segment a name.

27
00:01:49,000 --> 00:01:53,000
Click "Save As."

28
00:01:53,000 --> 00:01:57,000
If at any given time, I want 

29
00:01:57,000 --> 00:02:01,000
to get back to the segment, or hide it, I can click "Hide"

30
00:02:01,000 --> 00:02:05,000
or "Edit." And I can click on the segment dropdown to select other segments.

31
00:02:05,000 --> 00:02:09,000
And that's how you create Segments in Analytics for Apps.

