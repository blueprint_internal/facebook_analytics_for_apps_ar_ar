﻿1
00:00:05,000 --> 00:00:09,000
É fácil configurar um segmento no

2
00:00:09,000 --> 00:00:13,000
Facebook Analytics para Aplicativos em diferentes relatórios. Vou demonstrar como configurar

3
00:00:13,000 --> 00:00:17,000
um segmento no relatório “Receita”.

4
00:00:17,000 --> 00:00:21,000
Clico em “Receita”.

5
00:00:21,000 --> 00:00:25,000
Vejo que o relatório não está filtrado no momento 

6
00:00:25,000 --> 00:00:29,000
e que mostra 100% dos usuários. Mas posso aplicar um segmento 

7
00:00:29,000 --> 00:00:33,000
clicando em “Editar”.

8
00:00:33,000 --> 00:00:37,000
Depois, posso clicar em “Selecione a condição” e escolher

9
00:00:37,000 --> 00:00:41,000
entre diferentes condições, incluindo “Evento”, “Dados demográficos”, 

10
00:00:41,000 --> 00:00:45,000
“Informações do dispositivo” ou “Fonte de instalação”. Posso também adicionar 

11
00:00:45,000 --> 00:00:49,000
uma condição de percentil para filtrar com base

12
00:00:49,000 --> 00:00:53,000
nas pessoas que realizam mais determinada ação. Neste exemplo, vou criar uma condição 

13
00:00:53,000 --> 00:00:57,000
com base em dados demográficos.

14
00:00:57,000 --> 00:01:01,000
Seleciono um parâmetro 

15
00:01:01,000 --> 00:01:05,000
de “País”. Clico no campo 

16
00:01:05,000 --> 00:01:09,000
e seleciono “Estados Unidos da América”.

17
00:01:09,000 --> 00:01:13,000
Depois, clico em “Adicionar percentil”.

18
00:01:13,000 --> 00:01:17,000
Depois disso, seleciono o evento no qual pretendo basear o percentil.

19
00:01:17,000 --> 00:01:21,000
Por exemplo, pessoas que iniciam o aplicativo 

20
00:01:21,000 --> 00:01:25,000
e que fazem parte dos primeiros 10%.

21
00:01:25,000 --> 00:01:29,000
Em seguida, clico em “Aplicar”.

22
00:01:29,000 --> 00:01:33,000
Agora, podemos ver que o gráfico está sendo atualizado

23
00:01:33,000 --> 00:01:37,000
para mostrar os dados para esse segmento. 

24
00:01:37,000 --> 00:01:41,000
O segmento ainda não foi salvo. 

25
00:01:41,000 --> 00:01:45,000
Então, clico em “Mais” e seleciono “Salvar como”. Agora, posso dar um nome 

26
00:01:45,000 --> 00:01:49,000
ao meu segmento.

27
00:01:49,000 --> 00:01:53,000
Clico em “Salvar como”.

28
00:01:53,000 --> 00:01:57,000
Se, a qualquer momento, eu quiser voltar 

29
00:01:57,000 --> 00:02:01,000
ao segmento ou ocultá-lo, posso clicar em “Ocultar”

30
00:02:01,000 --> 00:02:05,000
ou em “Editar”. E posso clicar na lista suspensa para selecionar outros segmentos.

31
00:02:05,000 --> 00:02:09,000
É assim que se cria segmentos no Analytics para Aplicativos.