﻿1
00:00:05,000 --> 00:00:09,000
Rất dễ để thiết lập phân đoạn trong

2
00:00:09,000 --> 00:00:13,000
Phân tích ứng dụng trên Facebook, trong nhiều báo cáo khác nhau. Tôi sẽ hướng dẫn bạn

3
00:00:13,000 --> 00:00:17,000
cách thiết lập phân đoạn trong báo cáo "Doanh thu".

4
00:00:17,000 --> 00:00:21,000
Khi nhấp vào "Doanh thu",

5
00:00:21,000 --> 00:00:25,000
tôi có thể thấy rằng hiện tại báo cáo hoàn toàn chưa được lọc 

6
00:00:25,000 --> 00:00:29,000
và đang hiển thị 100% người dùng. Tuy nhiên, tôi có thể 

7
00:00:29,000 --> 00:00:33,000
áp dụng phân đoạn bằng cách nhấp vào "Chỉnh sửa".

8
00:00:33,000 --> 00:00:37,000
Sau khi nhấp vào "Chỉnh sửa", tôi có thể nhấp vào "Chọn điều kiện" rồi chọn trong số

9
00:00:37,000 --> 00:00:41,000
các loại điều kiện khác nhau, bao gồm "Sự kiện", "Nhân khẩu học", 

10
00:00:41,000 --> 00:00:45,000
"Thông tin thiết bị" hoặc "Nguồn cài đặt". Tôi cũng có thể 

11
00:00:45,000 --> 00:00:49,000
thêm điều kiện phần trăm vào bộ lọc dựa trên

12
00:00:49,000 --> 00:00:53,000
những người thực hiện hành động nhất định nhiều nhất. Trong ví dụ này, tôi sẽ 

13
00:00:53,000 --> 00:00:57,000
tạo điều kiện dựa trên thông tin nhân khẩu học.

14
00:00:57,000 --> 00:01:01,000
Tôi sẽ chọn thông số là 

15
00:01:01,000 --> 00:01:05,000
"Quốc gia". Tôi sẽ 

16
00:01:05,000 --> 00:01:09,000
nhấp vào trường và chọn "Hợp chủng quốc Hoa Kỳ".

17
00:01:09,000 --> 00:01:13,000
Sau đó, tôi sẽ nhấp vào "Thêm phần trăm".

18
00:01:13,000 --> 00:01:17,000
Sau khi nhấp, tôi sẽ chọn sự kiện mình muốn đặt làm cơ sở cho số phần trăm.

19
00:01:17,000 --> 00:01:21,000
Giả sử như những người khởi chạy ứng dụng, 

20
00:01:21,000 --> 00:01:25,000
và nằm trong 10% số người hàng đầu.

21
00:01:25,000 --> 00:01:29,000
Tiếp theo, tôi sẽ nhấp vào "Áp dụng".

22
00:01:29,000 --> 00:01:33,000
Lúc này, bạn có thể thấy biểu đồ đang

23
00:01:33,000 --> 00:01:37,000
cập nhật để hiển thị cho tôi dữ liệu của phân đoạn này. 

24
00:01:37,000 --> 00:01:41,000
Phân đoạn này vẫn chưa được lưu, vì vậy tôi có thể 

25
00:01:41,000 --> 00:01:45,000
nhấp vào "Khác" rồi chọn "Lưu dưới dạng". Bây giờ, tôi có thể 

26
00:01:45,000 --> 00:01:49,000
đặt tên cho phân đoạn.

27
00:01:49,000 --> 00:01:53,000
Nhấp vào "Lưu dưới dạng".

28
00:01:53,000 --> 00:01:57,000
Nếu vào bất kỳ thời điểm nào, tôi muốn 

29
00:01:57,000 --> 00:02:01,000
quay lại hoặc ẩn phân đoạn này, tôi có thể nhấp vào "Ẩn"

30
00:02:01,000 --> 00:02:05,000
hoặc "Chỉnh sửa". Sau đó tôi có thể nhấp vào menu phân đoạn thả xuống để chọn các phân đoạn khác.

31
00:02:05,000 --> 00:02:09,000
Và đó là cách tạo phân đoạn trong Phân tích ứng dụng.